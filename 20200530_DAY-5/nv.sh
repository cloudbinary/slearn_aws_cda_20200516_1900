#!/bin/bash 
# Update the Repository
sudo apt-get update 
# Install Utility Softwares
sudo apt-get install vim curl wget unzip elinks tree git -y 
# Download, Install & Configure WebServer i.e. Apache - apache2 
sudo apt-get install apache2 -y 
# Enable the Daemon / Service at Boot Level 
sudo systemctl enable apache2.service 
# Start the Daemon / Service 
sudo systemctl start apache2.service 
# Deploy a Sample Html File part of DocumentRoot i.e  /var/www/html/
echo "<html><body><h1>Region - North Virginia</h1></body></html>" > /var/www/html/index.html
# Restart the Daemon / Service 
sudo systemctl restart apache2.service 

